# libCube #

[![Join the chat at https://gitter.im/leduc1984/2.5d-pokemon](https://badges.gitter.im/leduc1984/2.5d-pokemon.svg)](https://gitter.im/leduc1984/2.5d-pokemon?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

View Project:
[Live Demo](http://cubepokemon.aerobatic.io/) 


### README ###

### What do I need to get started? ###

* A webserver (i.e nginx, apache, xampp) --or a browser with XHR cross-origin disabled
* A modern web-browser (Chrome, Safari, Opera work best)
* Tiled Map Editor (www.mapeditor.org)
* A love of pokemon


### Officially Supported Deployment Platforms ###

* Mac
* PC
* iOS
* Android


